//
//  Model.swift
//  School
//
//  Created by Mona on 12/27/22.
//

import Foundation


struct School: Codable{
   
    let school_name: String
    let dbn: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String

}
