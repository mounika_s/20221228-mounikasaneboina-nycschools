//
//  Service.swift
//  School
//
//  Created by Mona on 12/27/22.
//
import Foundation

struct Service {
    var schoolname = [School]()
    func service(){
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        
        URLSession.shared.dataTask(with: url!) { data, response, error in
            
            if error != nil {
                print(error?.localizedDescription)
                return
            }
            do {
                
          let result = try JSONDecoder().decode(School.self, from: data!)
               
                print(result)
                
            } catch {
        
            }
        
        }.resume()
        
}
}
