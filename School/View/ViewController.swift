//
//  ViewController.swift
//  School
//
//  Created by Mona on 12/27/22.
//

import UIKit

class ViewController: UIViewController{
    
    var schools = [School]()
    var sortedData = [School]()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    let session = URLSession.shared
    let searchController = UISearchController(searchResultsController:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
        //Search Controller
        searchController.delegate = self
        navigationItem.searchController = searchController
    
        
        //Create a URL String
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else {
            return
        }
        
        //Create a URLSession
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            
            guard let receivedData = data else {
                return
            }
            //calling parse method with json data
            self.parse(json:receivedData)
           
            //Reloading TableView in Main DispatchQueue
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
       
        task.resume()
        
    }

}

// MARK : SupportingMethods
extension ViewController {
    func parse(json: Data) {

        schools = try! JSONDecoder().decode([School].self, from: json)
        
        }
}

//Populating data into TableView
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)

        cell.textLabel?.text = schools[indexPath.row].school_name
        cell.textLabel?.font = UIFont(name:"Avenir", size:15)

        return cell
    
    }

    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let secondVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondVC") as! SecondVC

        secondVC.schoolObject = schools[indexPath.row]
        let backNavigationButton = UIBarButtonItem()
        backNavigationButton.title = "Schools"
        navigationItem.backBarButtonItem = backNavigationButton

        self.navigationController?.pushViewController(secondVC, animated: true)

    }
}

//Search and Sort
extension ViewController:  UISearchBarDelegate, UISearchControllerDelegate {
    
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
     sortedData = []
    for school in sortedData {
        let data = school.school_name.uppercased()
        if data.contains(searchText.uppercased()){
            sortedData.append(school)
        }
    }
      self.tableView.reloadData()
    }
}

