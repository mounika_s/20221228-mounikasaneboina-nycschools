//
//  SecondVC.swift
//  School
//
//  Created by Mona on 12/27/22.
//

import Foundation
import UIKit

class SecondVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var schoolObject: School?
    
    var school:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        if let dbn =  schoolObject?.dbn,
           let math = schoolObject?.sat_math_avg_score,
           let  writing = schoolObject?.sat_writing_avg_score,
           let reading =  schoolObject?.sat_critical_reading_avg_score {
            
            school.append("Dbn Score: \(dbn)")
            school.append("Math Score: \(math)")
            school.append("Writing Score: \(writing)")
            school.append("Reading Score: \(reading)")
        }
        
    }

}

//Populating data into TableView
extension SecondVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return school.count
   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")

        cell.textLabel?.text = school[indexPath.row]
        cell.textLabel?.font = UIFont(name:"Avenir", size:15)
        
        return cell
  
    }

    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
}
